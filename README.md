## Kubuntu™ - install traditional *Firefox* script

### What is a script?

In this case "script" is short for "shell script".  

Wikipedia defines "shell script" as follows:  
"*A shell script is a computer program designed to be run by a Unix shell, a command-line interpreter.* […]"  
  
In Kubuntu™ by default we use *Bash* (an acronym for the "Bourne Again Shell") and you can use it with e.g. the *Konsole* terminal emulator to run a script.

---

### Purpose of this script

This is a little helper script for Kubuntu™ (22.04 & later) users who want to install the traditional *Firefox* from Mozilla.org (the way Debian suggests) and use it complementary to or instead of the *Firefox Snap*.  
It can also remove the traditional *Firefox*, if it has been installed by this script before, and has been tested many times with Kubuntu™ 22.04 LTS up to 24.04 LTS.  

I hope you find the script useful! Yours respectfully, Schwarzer Kater

---

### Installation and usage of this script

**1.** To **download the script** click on its name in the file list above and on the next page use the download button on the far right side of the script name.  
***OR***  
**Copy and paste the code of the script into a blank *Kate* text editor document** and save it as "Kubuntu_install_traditional_Firefox.sh" to e.g. your home directory.

**2.** **Make the file *Kubuntu_install_traditional_Firefox.sh* executable in the *Dolphin* file manager**:  
go to the directory you downloaded/saved the script to, right-click on the file and choose *Properties* --> *Permissions* --> enable *Is executable* --> choose *OK*.  
***OR***  
**Make the file *Kubuntu_install_traditional_Firefox.sh* executable in the *Konsole* terminal emulator**:  
go to the directory you downloaded/saved the script to (`cd [path to]/[name of directory]`, e.g. `cd ~/Downloads`) and enter:  
`chmod +x Kubuntu_install_traditional_Firefox.sh`.

**3.** To **run the script in the *Konsole* terminal emulator** go to the directory you downloaded/saved the script to and enter:  
`./Kubuntu_install_traditional_Firefox.sh`.


> #### Important  
>- It is advised to run this script after a full upgrade of the system (either with *Discover* or in *Konsole* with `sudo apt update && sudo apt full-upgrade`) AND a reboot.  
   > - *Firefox* from Mozilla.org can simply be used alternatively with or without the *Firefox Snap* and no further actions are to be taken.  
   It will remain installed when release-upgrading (e.g. from 23.10 to 24.04 LTS) and causes no conflicts during the upgrade process.  
   This version of *Firefox* behaves roughly like the ones for macOS™ or Windows™ do and updates itself independently from *Discover* or APT package management (and can ask you before updating - if you would prefer being asked see the *Firefox* settings).

   You will be asked if you want [1] to download and install *Firefox* from Mozilla.org OR [2] to remove a previously installed *Firefox*, but if you choose [3] to cancel, no changes at all are made to your system.  
   After you chose [1] to install *Firefox* from Mozilla.org, you will be asked to select your preferred language next.  

For some more information about what is done in detail see the comments within the script itself.

---

### Known issues / bugs

- **Don't start and use both** the traditional *Firefox* **and** the *Firefox Snap* **simultaneously** - this can lead to problems!

- In very rare cases the compressed binary from Mozilla's server was either temporarily not available in a particular language or was corrupted (it once happened with "English (South African) lang=en-ZA" for example).  
The script will inform you about this and suggest the following solution.  
**--> The solution** is to run the script again, choosing a more "common" (but nonetheless fitting) language version like "English (US) lang=en-US" instead - which then has always worked fine.  
After the installation you can change to your preferred language in *Firefox* when it starts for the first time or later on in the *Firefox* settings.  

Bugs and suggestions can be posted here: [Kubuntu Forums .NET - Script to install traditional Firefox](https://www.kubuntuforums.net/forum/general/miscellaneous/coding-scripting/669543-script-to-install-traditional-firefox "https://www.kubuntuforums.net/forum/general/miscellaneous/coding-scripting/669543-script-to-install-traditional-firefox")

---

### Other Ubuntu™-based distributions

No other Ubuntu™-based distributions/flavours but Kubuntu™ 22.04 LTS & later are officially supported.  

Due to the amount of time it would take to thoroughly test this script with other distributions/flavours I will probably also not support them in the future.

That said the script should work with other official flavours of Ubuntu™ and with Debian so far.

---

### Acknowledgment

My appreciation to the administrators and members of [Kubuntu Forums .Net](https://www.kubuntuforums.net "https://www.kubuntuforums.net") who have inspired me and helped to develop, test and improve this script.

---

### Disclaimer / License

My scripts are in no way associated with Canonical™, Ubuntu™ or Kubuntu™. 

This script comes with ABSOLUTELY NO WARRANTY OF ANY KIND.  

It may be used, shared, copied and modified freely.

See [The Unlicense](https://gitlab.com/scripts94/kubuntu-get-rid-of-snap/-/blob/main/LICENSE "LICENSE") at the top.
